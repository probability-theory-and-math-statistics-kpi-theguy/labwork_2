package com.kpi;


import java.util.Random;

/**
 * Created by Yuriy on 25.09.2016.
 */

/**
 * function of exponential distribution of numbers
 */
public class Exponential {
    /**
     * @param lyambda;
     * Number, which determines the distribution
     */
    private double lyambda;

    //Getter
    public double getLyambda() {
        return lyambda;
    }
    //Setter
    public void setLyambda(double lyambda) {
        this.lyambda = lyambda;
    }


    public Exponential(double ly){
        setLyambda(ly);
    }

    /**
     * Method that determines the next number in the distribution
     * The number ri is a pseudo random number from interval from 0 to 1
     * @return
     * The output is a float number, which depends on this type of distribution
     */
    public float GenerateRi(){
        Random rand = new Random();
        float ri = rand.nextFloat();
        float Res = 0;
        Res = (float) ((-1/lyambda)*Math.log(ri));
        return Res;
    }
}
