package com.kpi;

/**
 * Created by Yuriy on 25.09.2016.
 */

/**
 * Class of characteristics of given numbers
 */
public class Characteristics {
    /**
     * @param testNumbers
     * Array of float numbers, obtained from generation of exponential distribution
     * @param math
     * This field keeps variable, resulting in the calculation of mathematical expectation
     */
    private float[] testNumbers;
    private float math;

    //Getter
    public float[] getTestNumbers() {
        return testNumbers;
    }

    //Setter
    public void setTestNumbers(float[] testNumbers) {
        this.testNumbers = testNumbers;
    }

    public Characteristics(float[] tests){
        setTestNumbers(tests);
    }

    /**
     * Method of experimental calculation of expectation that consists of calculation the arithmetic mean of the array
     * @return
     * Obtaining the number, which passes to calculate the dispersion
     */
    public float mathExpect(){
        float sum = 0;
        for(int i = 0; i < testNumbers.length; i++){
            sum += testNumbers[i];
        }
        math = sum/testNumbers.length;
        return math;
    }

    /**
     * Method that calculates experimental the value of dispersions and standard deviation
     * @return
     * Obtaining the number that expresses the experimental number
     */
    public float dispersion(){
        float sum = 0;
        for(int i = 0; i < testNumbers.length; i++){
            sum += Math.pow(testNumbers[i] - math, 2);
        }
        return (float) Math.sqrt(sum/testNumbers.length);
    }

    /**
     * A special method that calculates the theoretical values of this characteristics
     * @param l
     * A number that characterize of exponential distribution
     * @return
     * Returns the theoretical value of this values
     */
    public float theoretical(double l){
        return (float) (1/l);
    }
}
