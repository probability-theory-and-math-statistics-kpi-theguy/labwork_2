package com.kpi;

import java.util.Scanner;
import java.io.*;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scan = new Scanner(in);
        out.println("We use exponential distribution. Enter the number of lyambda:");
        double l = scan.nextDouble();
        out.println("Now enter the number of samples:");
        int n = scan.nextInt();
        Exponential exp = new Exponential(l);
        float[] numbers = new float[n];
        for (int i = 0; i < n; i++){
            numbers[i] = exp.GenerateRi();
        }
        out.println("Our array with size of "+n+" is saved in file: С://generator1.txt");
        File fil = new File("A://generator1.txt");
        try {
            if(!fil.exists()){
                fil.createNewFile();
            }
            BufferedWriter buff = new BufferedWriter(new FileWriter(fil));
            for(int i = 0; i < numbers.length; i++){
                buff.write(numbers[i] + " ");
            }
            buff.flush();
            buff.close();
        }catch(IOException e){
            err.println(e.getMessage());
        }
        Characteristics chart = new Characteristics(numbers);
        out.println("Now, let's see the characteristics of this array.");
        out.println("Experimental value of mathematical expectetion is:" + chart.mathExpect());
        out.println("Theoretical is:" + chart.theoretical(l));
        out.println("Experimental value of standart deviation is:" + chart.dispersion());
        out.println("Theoretical is:" + chart.theoretical(l));
    }
}
